module.exports = function(args) {
	var fs = require('fs');
	var pathM = require('path');
	var path = process.argv[2];
	var ext = process.argv[3];
	fs.readdir(path, function(err, data) {
		data.forEach(function(file) {
			if (pathM.extname(file) === "."+ext) {
				console.log(file);
			}
		});
	});
}
