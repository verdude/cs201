var express = require('express');
var router = express.Router();
var fs = require('fs');

var jokes = [];
var currId = 600;

/* GET home page. */
router.get('/', function(req, res, next) {
	if (jokes.length === 0) {
		fs.readFile(__dirname + '/jokes.json', "utf-8", function(err,data) {
			if(err) throw err;
			jokes = JSON.parse(data);
		});
	} 
	res.render('index', { jokes: jokes });
});

router.get('/upvote', function(req, res, next) {
	if (req.query.id >= jokes.length) {
		res.sendStatus(400);
	} else {
		// add to the joke's count
		jokes[req.query.id].votes++;
		res.sendStatus(200);
	}
});

router.get('/createJoke', function(req, res, next) {
	res.render('createJoke', {test:"hi"});
});

router.post('/addJoke', function(req, res, next) {
	var body = req.body,
		categories = body.categories,
		joke = body.joke,
		name = body.name;
	if (body.name.length < 4 || body.name.length > 10) {
		res.sendStatus(400);
	} else if (joke.length < 3 || joke.length > 200) {
		res.sendStatus(400);
	} else {
		// add a joke to the jokes list
		jokes.unshift({
			id: currId,
			votes: 0,
			categories: categories.split(",").map(function(l){return l.trim();}),
			joke: joke,
			name: name
		});
		currId++;
		res.redirect('/');
	}
});

module.exports = router;
