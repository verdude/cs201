var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('<script type="text/javascript">window.location="/"</script>');
});

module.exports = router;
