

var app = angular.module("app", []);

app.controller("jokesListCtrl", function($scope){
    $scope.allJokes = window.jokes;
    $scope.jokesList = $scope.allJokes.slice(0, 30);

    $scope.upvote = function(index) {
        
        $.ajax({
            url: "/upvote?id=" + index,
            success: function(data, status) {
                var jokeId = $scope.jokesList[index].id;
                $scope.jokesList.votes++;
                $scope.allJokes.some(function(j){
                    // update the votes without reloading
                    if (j.id === jokeId) {
                        $scope.allJokes[$scope.allJokes.indexOf(j)].votes++;
                        return true;
                    } else return false;
                });
            }
        });
    };
});

/********************************
        Joke directive
 *******************************/
app.directive("joke", function($compile){
    return {
      scope: {
        joke: '=',
        onUpvote: '&'
      },
      restrict: 'E',
      template: (
        '\
        <div class="Joke">\
            <div class="index">{{index}}.</div>\
            <img src="https://news.ycombinator.com/grayarrow.gif" ng-click="onUpvote({i: index})">\
            <div class="jokeContent">{{joke.joke}}</div>\
            <div class="bottomHalf">\
                <div class="votes">points: {{joke.votes}}</div>\
                <div class="id">id: {{joke.id}}</div>\
                <div class="categories">categories: {{joke.categories}}</div>\
                <div class="author">author: {{joke.name || "icndb"}}</div>\
            </div>\
        </div>'
      ),
      link: function(scope, element, attrs) {
        scope.index = parseInt(attrs.index)+1;
      }
    };
});