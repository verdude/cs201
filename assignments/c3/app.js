
var app = angular.module("app", ["chart.js"]);

// app.directive("candidate", candidateDirective);

// candidate controller
app.controller("candidateCtrl", function($scope){
  $scope.democrats = [];
  $scope.republicans = [];
  // update the vote count
  // then remove the ability to vote via localstorage
  $scope.vote = function(candidate){
    if (localStorage.getItem("voted") !== null) {
      console.log("user marked as already voted");
      return;
    }
    candidate.votes += 1;
    $.ajax({
        type : "POST",
        url : "https://bananaforscale.io/write.php",
        dataType : 'json',
        data : {
            json : "loadInfo("+JSON.stringify(toJson($scope.democrats, $scope.republicans))+");"
        },
        success: function(data) {
          // show popup
          document.getElementById('light').style.display='block';
          document.getElementById('fade').style.display='block';
          // mark as voted
          localStorage.voted = "true";
          // redirect
          setTimeout(function(){
            location.reload();
          }, 2000);
        }
    });
  };
  // initially load the candidate info onto the page
  $scope.load = function(json){
    json.democrats.forEach(function(c){
      $scope.democrats.push(c);
    });
    json.republicans.forEach(function(c){
      $scope.republicans.push(c);
    });
  };
});

// Pie chart controller
app.controller("PieCtrl", function ($scope) {
  $scope.rlabels = [];
  $scope.rdata = [];

  $scope.blabels = [];
  $scope.bdata = [];
  
  $scope.dlabels = [];
  $scope.ddata = [];

  $scope.setup = function(json) {
    $scope.rlabels = json.republicans.map(function(r){return r.name;});
    $scope.rdata = json.republicans.map(function(r){return r.votes;});

    $scope.dlabels = json.democrats.map(function(d){return d.name;});
    $scope.ddata = json.democrats.map(function(d){return d.votes;});

    var dvotes = json.democrats.map(function(d){return d.votes;});
    var rvotes = json.republicans.map(function(r){return r.votes;});

    $scope.blabels = ["Democrats", "Republicans"];
    $scope.bdata = [dvotes.reduce(function(a,b){return a+b;},0), rvotes.reduce(function(a,b){return a+b;}, 0)];
  };
});

// serialize js arrays into one object
function toJson(dem, rep) {
  return {
    "democrats": dem,
    "republicans": rep
  };
}


// Do the inital page setup by loading the candidate information into the pie charts and the candidate lists
function loadInfo(json) {
  //CORS workaround
  var candidateCtrl = angular.element(document.getElementById('body')).scope();
  candidateCtrl.$apply(candidateCtrl.load(json));

  var pieCtrl = angular.element(document.getElementById('charts')).scope();
  pieCtrl.$apply(pieCtrl.setup(json));
}

// do the initial setup call with jsonp
// jsonp should be set up to send itself to the loadInfo function
$.ajax({
  type: 'GET',
  url: "https://bananaforscale.io/cs201/assignments/c3/ajax/info.json",
  contentType: "application/json",
  dataType: 'jsonp'
});

// Non-functional directive
function candidateDirective() {
  return {
    scope: {
      user: '='
    },
    restrict: 'E',
    template: (
      '\
      <div class="candidate">\
        <img ng-src="{{candidate.url}}">\
        <h6>{{candidate.name}}</h6>\
      </div>'
    )
  };
}